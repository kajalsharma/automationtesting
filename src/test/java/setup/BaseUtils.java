package setup;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.github.bonigarcia.wdm.WebDriverManager;
import page.MakePayment;
public class BaseUtils {

	public static RemoteWebDriver driver;
	private static MakePayment mp;
	public static String Browser;
	private static Properties config;
	private static String directory;

	/**
	 * BaseUtils() is a constructor
	 * @param driver1
	 */
	public BaseUtils(RemoteWebDriver driver1) {
		driver = driver1;
		PageFactory.initElements(driver, this); 
		initBaseUtils(driver);
	}
	/** 
	 * initBaseUtils() method contains set of instruction to Initialize the BaseUtils objects
	 * @return Nothing.
	 **/	
	public static void initBaseUtils(RemoteWebDriver driver2) {
		mp = new MakePayment(driver2);
	}
	/** 
	 * getDriver() is getter method of WebDriver object
	 *  @return WebDriver object.
	 **/
	public static RemoteWebDriver getDriver()
	{
		return driver;
	}
	/** 
	 * getmakepayment() is getter method of Actions object
	 * @return action object.
	 **/	
	public static MakePayment getmakepayment()	 {
		return mp; 
	}

	/**************************************************All Set Up Methods below **********************************

	/** 
	 * intializeDriver() method contains set of instruction to 
	 * Start the particular Browser and returns the driver  
	 * @throws  IOException 
	 * @param  String Browser
	 * @return 
	 * @return RemoteWebDriver
	 **/
	public static RemoteWebDriver intializeDriver() {
		String headless;
		try {
			Browser = InitProperties("browser.name");
			headless = InitProperties("mode.headless");
			if(Browser.equalsIgnoreCase("IE"))
			{
				//System.setProperty("webdriver.ie.driver",CommonUtils.getPresentWorkingDir()+CommonUtils.projName+"Project"+File.separator +"lib"+ File.separator +"IEDriverServer.exe");
				System.setProperty("webdriver.ie.driver",getPresentWorkingDir()+File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"IEDriverServer.exe");

				//DesiredCapabilities cap=DesiredCapabilities.internetExplorer();
				//driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),cap);			

				DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
				caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);

				caps.setCapability("ignoreProtectedModeSettings", true);
				//caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				//caps.setJavascriptEnabled(true);
				//caps.setCapability("requireWindowFocus", false);
				//caps.setCapability("enablePersistentHover", false);
				//caps.setCapability("ignoreZoomSetting", true);
				//caps.setCapability("nativeEvents",false);

				driver=new InternetExplorerDriver(caps);

				//driver=new InternetExplorerDriver();

			}
			else if(Browser.equalsIgnoreCase("Firefox"))
			{
				//DesiredCapabilities cap=DesiredCapabilities.firefox();
				//driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),cap);
				driver = new FirefoxDriver();
			} 
			else if(Browser.equalsIgnoreCase("Chrome"))
			{
				System.setProperty("webdriver.chrome.driver",getPresentWorkingDir()+File.separator+"src"+File.separator+"test"+File.separator+"java"+File.separator+"external"+File.separator+"chromedriver.exe");
				//DesiredCapabilities cap=DesiredCapabilities.chrome();
			   WebDriverManager.chromedriver().proxyUser(System.getenv("HTTPS_PROXY_USER")).proxyPass(System.getenv("HTTPS_PROXY_PASS")).proxy(System.getenv("HTTPS_PROXY_NAME")).setup();
				//				WebDriverManager.chromedriver().setup();
				//driver = new RemoteWebDriver(cap);
				ChromeOptions o = new ChromeOptions();
				o.addArguments("disable-extensions");
				o.addArguments("--disable-notifications");
				o.addArguments("incognito");

				//--below code is for headless execution 

				if(headless.equalsIgnoreCase("true")) {
					o.addArguments("headless");
					o.addArguments("window-size=1382x744");
					//o.addArguments("--start-maximized");
				}

				//--NEW CODE : set path for downloading files
				String downloadFilepath = getPresentWorkingDir()+File.separator+"DownloadFiles";
				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
				chromePrefs.put("download.default_directory", downloadFilepath);
				o.setExperimentalOption("prefs", chromePrefs);

				driver = new ChromeDriver(o);
				//	o.addArguments("headless");
				//	o.addArguments("window-size=1200x600");
				//	o.addArguments("--start-maximized");
			}
		} catch (Exception e) {
			System.err.println("Exception occured in BaseUtils- intializeDriver() :"+e.getLocalizedMessage());
		}finally {
			System.out.println("Executed the Browser Setup Block");
		} 

		return driver;
	}
	/** 
	 * InitProperties() method to read key, values from the configuration files.
	 * @param toread = Property name to read
	 * @return value of the string to return
	 **/
	public static String InitProperties(String toread) {
		config = readProperties("configuration.properties",File.separator+"Configs");
		String value = config.getProperty(toread);
		return value;
	}
	/** 
	 * getPresentWorkingDir() method to get the Working Directory Path
	 * @param path = User directory path
	 * @return path
	 **/
	public static String getPresentWorkingDir()	throws IOException	{
		String path;
		path = System.getProperty("user.dir");
		return path;
	}
	/** 
	 * getPresentWorkingDir() method to get the Working Directory Path
	 * @param path = User directory path
	 * @return path
	 **/
	public static  Properties readProperties(String Filename, String Folder){
		Properties props = new Properties();
		try {
			File config_file = new File(Folder + File.separator + Filename);
			directory = getPresentWorkingDir();
			config_file = new File(directory + File.separator + Folder + File.separator + Filename);
			if (config_file == null) {
				System.out.println("Couldn't find " + Filename + ", Due to this the rest of the tests will fail");
			}
			props.load(new FileInputStream(config_file));
		} catch (FileNotFoundException e) {
			System.out.println(e.getLocalizedMessage());
		} catch (IOException e) {
			System.out.println(e.getLocalizedMessage());
		}
		return props;
	}
	/**
	 * fn_verifyinlist(List<WebElement> webElement, String objName, String Value, ExtentTest logTestReport)
	 * @author V7008074(Vasavi)
	 *@Description : This function will verify the provided value in provided list
	 * @param logTestReport
	 * @param webElement : objlist ex: getmailid() 
	 * @param objName : name of object ex: MailID
	 * @param Value : value - which has to be verified in a getmailid()list
	 * @param maxSwipeCount : maximum number of times that next button has to be clicked
	 * @return 
	 * @throws InterruptedException 
	 * @throws Exception 
	 * @ predefined : 
	 */
	public static void fn_VerifyInList(List<WebElement> webElement,String objName, String Value, ExtentTest logTestReport) throws InterruptedException {
		boolean flag = false ;
		Thread.sleep(1000);
		try {int k=0;
		if(webElement.get(0).getText().equalsIgnoreCase(Value))
			logTestReport.log(Status.PASS, Value+" is available in first Count of "+objName+ "list");
		else {
			do {
				for(int i=0;i<=webElement.size()-1;i++){
					System.out.println(webElement.get(i).getText());
					if(webElement.get(i).getText().equalsIgnoreCase(Value)){
						flag = true;
						logTestReport.log(Status.PASS, "Verified Successfully !! "+Value+" is available in "+objName+ " list. It is not available in top most record of the list").addScreenCaptureFromPath(captureScreenShot("ProductList"));
						break;}
					}
				if (!flag) 
					logTestReport.log(Status.INFO, "Value not present on the log");
			}while(!flag);
		}
		}
		catch (Exception e) {
			if(e.getLocalizedMessage().contains("Index: 0"))
				logTestReport.log(Status.INFO, "Caught Exception in the method");
		}
 }
	/**
	 * captureScreenShot(String fileName)  : Will take the screen shot of current highlighted page
	 * @param String fileName
	 * @return String - will return of path of snapshot where it got saved
	 * @throws Exception
	 */
	public static String captureScreenShot(String fileName){
		String dest = null;
		try	{
			if(fileName.length()>5)
				fileName = fileName.substring(0,5);
			File scrFile = (File)((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			dest = getPresentWorkingDir() + File.separator +"test-reports" +File.separator+"ScreenShots" + File.separator + fileName + "_" + printTime() + ".jpg";
			File destFile = new File(dest);
			FileUtils.copyFile(scrFile, destFile);
		}
		catch (Exception e) {
			System.out.println("Caught Exception in the method"+e.getLocalizedMessage());
		}
		return dest;
	}
	/**
	 * Prints current time stamp in the format yyyyMMdd_HHmmssSSS
	 * @return String
	 */
	public static String printTime(){
		@SuppressWarnings("unused")
		Calendar cal = Calendar.getInstance();
		String timeStamps = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(Calendar.getInstance().getTime());
		return timeStamps;
	}
}
