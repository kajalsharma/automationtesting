package setup;

import java.awt.AWTException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentTest;

public class TestSetup {
	protected static RemoteWebDriver driver;
	public static String applicationURL;
	public static ExtentTest logTestReport;
	@BeforeSuite
	public void beforeSuite() throws IOException {	// creating reporting folder and files before test suit execution
		driver = BaseUtils.intializeDriver();
		new BaseUtils(driver);
	}
	@BeforeClass(alwaysRun=true)
	public void StartTestCase() throws Throwable
	{	
		applicationURL = BaseUtils.InitProperties("url");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.get(applicationURL);
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		System.out.println("Title of Page:" + driver.getTitle());
		System.out.println("Executed the Before Test Case");
	}
	@AfterSuite(alwaysRun = true)
	public void quitDriver() throws InterruptedException,IOException, AWTException {
		System.out.println("Quit the Driver - After Suite Method");
		driver.quit();	
	}
}