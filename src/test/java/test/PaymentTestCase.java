package test;
/* Author Name		: 	Kajal Sharma
 * Creation Date	: 	13-07-2020
 * Epic/User story	: 	Interview Test Sample
 * Test case name	:   Complete flow of Product from Selecting to Purchasing it.
 * Prerequisite 	:   Application Url navigation.
 * Description		:   Validating complete process of any product from selection till purchasing it.
 * Tests Covered    :	a) visit ://weathershopper.pythonanywhere.com/
						b) Get the temperature
						c) Based on temperature choose to buy sunscreen or moisturizer
						d) If you choose sunscreen, then read Instructions and then add product accordingly
						e) If you choose moisturizer, then read Instructions and then add product accordingly
						f) Verify the cart
						g) Make payment
						h) verify successMessage.
 * 						
 * */
import java.lang.reflect.Method;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import page.MakePayment;
import setup.*;

public class PaymentTestCase extends TestSetup{
	public static String wait;
	MakePayment mp;
	String exp1="Moisturizers";
	String exp2="Sunscreens";
	String exp3="Current temperature";
	
	@BeforeClass
	public void BeforeEachClass() throws Exception {
		mp = BaseUtils.getmakepayment();
	}
	
	@BeforeMethod
	public void BeforeEachMehtod(Method method) {
		System.out.println("Running the Test cases Before Method");
	}
	
	@Test(priority=1)
	public void TS_ValidateTheHomePage() {
		try {
			System.out.println("Executing the TS_ValidateTheHomePage Test Case");
			mp.validateThePage(exp1,logTestReport);
			mp.validateThePage(exp2,logTestReport);
			mp.validateThePage(exp3,logTestReport);
			logTestReport.log(Status.PASS, "Action is performed on the page");
			System.out.println("TC's executed successfully");
		}catch(Exception e) {
			System.out.println("Exception caught in TS_ValidateTheHomePage()" +e.getLocalizedMessage());
		}
	}
	
	@Test(priority=2)
	public void TS_PaymentMethod() {
		try {
			System.out.println("Executing the TS_PaymentMethod Test Case");
			mp.makePaymentValidationMethod(logTestReport);
			System.out.println("TC's executed successfully");
			logTestReport.log(Status.PASS, "Action is performed on the page");
		}catch(Exception p) {
			logTestReport.log(Status.WARNING, "Action failed on the page");
			System.out.println("Exception caught in TS_PaymentMethod()" +p.getLocalizedMessage());
		}
	}
	@AfterMethod
	public void mandatoryAfterMethod(ITestResult testResult) throws Exception
	{
	System.out.println("Printing After Method Class");
	}
	
	@AfterClass(alwaysRun=true)
	public void afterclass() {
		System.out.println("Testing");
	}
}